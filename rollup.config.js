import babel from 'rollup-plugin-babel'
import commonjs from 'rollup-plugin-commonjs'
import css from 'rollup-plugin-css-only'
import external from 'rollup-plugin-peer-deps-external'
import pkg from './package.json'
import resolve from 'rollup-plugin-node-resolve'
import url from 'rollup-plugin-url'

export default {
  input: 'src/index.js',
  output: [
    {
      file: pkg.main,
      format: 'cjs',
      sourcemap: true
    },
    {
      file: pkg.module,
      format: 'es',
      sourcemap: true
    }
  ],
  plugins: [
    external(),
    css({ output: 'dist/bundle.css' }),
    url(),
    babel({
      exclude: 'node_modules/**',
      plugins: [ 'external-helpers' ]
    }),
    resolve(),
    commonjs()
  ]
}
