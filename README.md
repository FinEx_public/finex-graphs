# finex-graphs

> D3 graphs

[![NPM](https://img.shields.io/npm/v/finex-graphs.svg)](https://www.npmjs.com/package/finex-graphs) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save finex-graphs
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'finex-graphs'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [finex](https://github.com/finex)
